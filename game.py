import random
import pygame

from config import (
    BLACK,
    WHITE,
    font,
    player_1_succ,
    player_2_succ,
    player_tie,
    next_turn,
)

from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
    RLEACCEL,
)

pygame.init()
pygame.font.init()
pygame.mixer.music.load('audio/Battleship.ogg')
pygame.mixer.music.play(-1)
DEATH_SOUND = pygame.mixer.Sound('audio/death.wav')


MYFONT = pygame.font.SysFont(font, 19)
ALERT = pygame.font.SysFont(font, 40)
# Defines height and width of the game screen
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800
SCREEN = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
# Forms group of all obstacle sprites( both moving and stationary)
ENEMIES = pygame.sprite.Group()
# Forms group of all sprites
ALL_SPRITES = pygame.sprite.Group()
# Will keep track of time elapsed since next round started
TIME = 0
# keeps track of frame of animation, the PLAYER animation is currently at
STATE = 0

# Class for PLAYERs


class Players(pygame.sprite.Sprite):
    def __init__(self, player_num):
        super(Players, self).__init__()
        self.player_id = player_num
        self.wins1 = 0
        self.wins2 = 0
        pic = pygame.image.load('images/Idle (1).png').convert()
        trans_color = pic.get_at((0, 0))
        pic = pygame.transform.smoothscale(pic, (35, 40))
        self.surf = pic
        self.surf.set_colorkey(trans_color, RLEACCEL)
        self.score1 = 0
        self.score2 = 0
        self.time1 = 0
        self.time2 = 0
        if player_num == 1:
            self.rect = pygame.Rect(
                (SCREEN_WIDTH - self.surf.get_width())/2,
                SCREEN_HEIGHT - self.surf.get_height(),
                35,
                40,
            )
        else:
            self.rect = pygame.Rect(
                (SCREEN_WIDTH - self.surf.get_width())/2, 0, 35, 40)

    # Moves PLAYER and check if he has reached end point
    def update(self, pressed_keys, state):
        temp_var = 0
        if pressed_keys[K_UP]:
            self.rect.move_ip(0, -2)
            temp_var += 1
        if pressed_keys[K_DOWN]:
            self.rect.move_ip(0, 2)
            temp_var += 1
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-2, 0)
            temp_var += 1
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(2, 0)
            temp_var += 1
        if temp_var > 0:
            path = 'images/Run (' + str((STATE//4) + 1) + ').png'
            pic = pygame.image.load(path).convert()
            state += 1
            if state >= 60:
                state = 0
            trans_color = pic.get_at((0, 0))
            pic = pygame.transform.smoothscale(pic, (35, 40))
            self.surf = pic
            self.surf.set_colorkey(trans_color, RLEACCEL)
        else:
            path = 'images/Idle (' + str((STATE//4) + 1) + ').png'
            pic = pygame.image.load(path).convert()
            state += 1
            if state > 59:
                state = 0
            trans_color = pic.get_at((0, 0))
            pic = pygame.transform.smoothscale(pic, (35, 40))
            self.surf = pic
            self.surf.set_colorkey(trans_color, RLEACCEL)

        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH

        # If below conditions fulfilled then player has reached end,
        # so reset obstacles( both if condition and condition in reset)
        if self.rect.top <= 0:
            self.rect.top = 0
            reset_1()

        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT
            reset_2()
        return state

# Class of all stationary obstacles


class StationaryObstacles(pygame.sprite.Sprite):

    def __init__(self, x_cord, y_cord):
        super(StationaryObstacles, self).__init__()
        pic_1 = pygame.image.load('images/wall.png').convert()
        trans_color = pic_1.get_at((0, 0))
        pic_1 = pygame.transform.scale(pic_1, (100, 60))
        self.surf = pic_1
        self.surf.set_colorkey(trans_color, RLEACCEL)
        self.rect = pygame.Rect((x_cord, y_cord, 100, 60))

# Class of all moving obstacles


class MovingObstacles(pygame.sprite.Sprite):
    def __init__(self, x_cord, y_cord, player):
        super(MovingObstacles, self).__init__()
        pic_1 = pygame.image.load('images/boat.png').convert()
        trans_color = pic_1.get_at((0, 0))
        pic_1 = pygame.transform.scale(pic_1, (100, 60))
        self.surf = pic_1
        self.surf.set_colorkey(trans_color, RLEACCEL)
        self.rect = pygame.Rect((x_cord, y_cord, 100, 60))
        # Pick random speed for every moving obstacle
        if player.player_id == 1:
            self.speed = random.randrange(16 + player.wins1, 22 + player.wins1)
        else:
            self.speed = random.randrange(16 + player.wins2, 22 + player.wins2)

    # Move each obstacle by corresponding speed,
    # and if it goes out of screen, delete it

    def update(self):
        self.rect.move_ip(self.speed, 0)
        if self.rect.right > (SCREEN_WIDTH + 100):
            self.kill()


pygame.display.set_caption('Shivam')

# Adding all stationary obstacles
OBJ = StationaryObstacles((SCREEN_WIDTH - 100)/2 - 200, SCREEN_HEIGHT - 184)
ENEMIES.add(OBJ)
ALL_SPRITES.add(OBJ)
OBJ = StationaryObstacles((SCREEN_WIDTH - 100)/2 + 200, SCREEN_HEIGHT - 184)
ENEMIES.add(OBJ)
ALL_SPRITES.add(OBJ)
OBJ = StationaryObstacles((SCREEN_WIDTH - 100)/2, SCREEN_HEIGHT - 344)
ENEMIES.add(OBJ)
ALL_SPRITES.add(OBJ)
OBJ = StationaryObstacles((SCREEN_WIDTH - 100)/2, SCREEN_HEIGHT - 660)
ENEMIES.add(OBJ)
ALL_SPRITES.add(OBJ)
OBJ = StationaryObstacles((SCREEN_WIDTH - 100)/2 - 200, SCREEN_HEIGHT - 504)
ENEMIES.add(OBJ)
ALL_SPRITES.add(OBJ)
OBJ = StationaryObstacles((SCREEN_WIDTH - 100)/2 + 200, SCREEN_HEIGHT - 504)
ENEMIES.add(OBJ)
ALL_SPRITES.add(OBJ)


# Update score of current PLAYER corresponding to his/her current position
def update_score(player):
    if player.player_id == 1:
        player.score1 = 0
        if player.rect.bottom < SCREEN_HEIGHT - 164:
            player.score1 += 20
        if player.rect.bottom < SCREEN_HEIGHT - 324:
            player.score1 += 15
        if player.rect.bottom < SCREEN_HEIGHT - 484:
            player.score1 += 20
        if player.rect.bottom < SCREEN_HEIGHT - 640:
            player.score1 += 15
        if player.rect.top < 24:
            player.score1 += 10
    if player.player_id == 2:
        player.score2 = 0
        if player.rect.top > SCREEN_HEIGHT - 164:
            player.score2 += 20
        if player.rect.top > SCREEN_HEIGHT - 324:
            player.score2 += 15
        if player.rect.top > SCREEN_HEIGHT - 484:
            player.score2 += 20
        if player.rect.top > SCREEN_HEIGHT - 640:
            player.score2 += 15
        if player.rect.bottom > SCREEN_HEIGHT - 24:
            player.score2 += 10

# Reset the game if PLAYER 2 died or reached the end


def reset_2():
    global TIME
    winner = 0
    if PLAYER.player_id == 2:
        PLAYER.player_id = 1
        PLAYER.time2 = TIME//60
        TIME = 0
        PLAYER.rect = pygame.Rect(
            (SCREEN_WIDTH - PLAYER.surf.get_width())/2,
            SCREEN_HEIGHT - PLAYER.surf.get_height(),
            35,
            40,
        )
        for obj in ENEMIES:
            if obj.__class__.__name__ == "MovingObstacles":
                obj.kill()
        st_time = pygame.time.get_ticks()

        # Pause game for 1 second and show the winner
        while pygame.time.get_ticks() < (st_time + 1000):
            if PLAYER.score1 > PLAYER.score2:
                winner = 1
                txt_to_show = ALERT.render(
                    player_1_succ, True, WHITE)
                SCREEN.blit(txt_to_show, (SCREEN_WIDTH /
                                          2 - 150, SCREEN_HEIGHT/2 - 30))
                pygame.display.flip()

            elif PLAYER.score1 == PLAYER.score2:
                if PLAYER.time1 == PLAYER.time2:
                    winner = 3
                    txt_to_show = ALERT.render(player_tie, True, WHITE)
                    SCREEN.blit(txt_to_show, (SCREEN_WIDTH /
                                              2 - 65, SCREEN_HEIGHT/2 - 30))
                    pygame.display.flip()
                elif PLAYER.time1 < PLAYER.time2:
                    winner = 1
                    txt_to_show = ALERT.render(
                        player_1_succ, True, WHITE)
                    SCREEN.blit(txt_to_show, (SCREEN_WIDTH /
                                              2 - 150, SCREEN_HEIGHT/2 - 30))
                    pygame.display.flip()
                else:
                    winner = 2
                    txt_to_show = ALERT.render(
                        player_2_succ, True, WHITE)
                    SCREEN.blit(txt_to_show, (SCREEN_WIDTH /
                                              2 - 150, SCREEN_HEIGHT/2 - 30))
                    pygame.display.flip()

            else:
                winner = 2
                txt_to_show = ALERT.render(
                    player_2_succ, True, WHITE)
                SCREEN.blit(txt_to_show, (SCREEN_WIDTH /
                                          2 - 150, SCREEN_HEIGHT/2 - 30))
                pygame.display.flip()
        if winner == 1 or winner == 3:
            PLAYER.wins1 += 1
        if winner == 2 or winner == 3:
            PLAYER.wins2 += 1
        PLAYER.score1 = 0
        PLAYER.score2 = 0
        TIME = 0

# Reset the game if PLAYER 1 just died or reached end.


def reset_1():
    global TIME
    if PLAYER.player_id == 1:
        PLAYER.time1 = TIME//60
        PLAYER.player_id = 2
        PLAYER.rect = pygame.Rect(
            (SCREEN_WIDTH - PLAYER.surf.get_width())/2, 0, 35, 40)
        for obj in ENEMIES:
            if obj.__class__.__name__ == "MovingObstacles":
                obj.kill()
        st_time = pygame.time.get_ticks()
        while pygame.time.get_ticks() < (st_time + 1000):
            txt_to_show = ALERT.render(
                next_turn, True, WHITE)
            SCREEN.blit(txt_to_show, (SCREEN_WIDTH /
                                      2 - 180, SCREEN_HEIGHT/2 -30))
            pygame.display.flip()

        TIME = 0


RUNNING = True
CLOCK = pygame.time.Clock()

# Adding events for adding moving obstacles, at random intervals,
# corresponding to each partition.
ADD1 = pygame.USEREVENT + 1
pygame.time.set_timer(ADD1, random.randrange(1000, 3000))
ADD2 = pygame.USEREVENT + 2
pygame.time.set_timer(ADD2, random.randrange(1000, 3000))
ADD3 = pygame.USEREVENT + 3
pygame.time.set_timer(ADD3, random.randrange(1000, 3000))
ADD4 = pygame.USEREVENT + 4
pygame.time.set_timer(ADD4, random.randrange(1000, 3000))
ADD5 = pygame.USEREVENT + 5
pygame.time.set_timer(ADD5, random.randrange(1000, 3000))

# Make PLAYER object with id 1 and add sprite
PLAYER = Players(1)
ALL_SPRITES.add(PLAYER)

# Add moving obstacles which will be released just once at start of game.
OBJ = MovingObstacles(-100, 692, PLAYER)
ENEMIES.add(OBJ)
ALL_SPRITES.add(OBJ)
OBJ = MovingObstacles(-100, 542, PLAYER)
ENEMIES.add(OBJ)
ALL_SPRITES.add(OBJ)
OBJ = MovingObstacles(-100, 376, PLAYER)
ENEMIES.add(OBJ)
ALL_SPRITES.add(OBJ)
OBJ = MovingObstacles(-100, 220, PLAYER)
ENEMIES.add(OBJ)
ALL_SPRITES.add(OBJ)
OBJ = MovingObstacles(-100, 62, PLAYER)
ENEMIES.add(OBJ)
ALL_SPRITES.add(OBJ)

while RUNNING:
    TIME = TIME + 1
    CLOCK.tick(60)
    for event in pygame.event.get():
        if event.type == QUIT:
            RUNNING = False
        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                RUNNING = False
        # Add moving obstacle in partition,
        # depending on if corresponding event is active
        if event.type == ADD1:
            OBJ = MovingObstacles(-100, 692, PLAYER)
            ENEMIES.add(OBJ)
            ALL_SPRITES.add(OBJ)
        if event.type == ADD2:
            OBJ = MovingObstacles(-100, 542, PLAYER)
            ENEMIES.add(OBJ)
            ALL_SPRITES.add(OBJ)
        if event.type == ADD3:
            OBJ = MovingObstacles(-100, 376, PLAYER)
            ENEMIES.add(OBJ)
            ALL_SPRITES.add(OBJ)
        if event.type == ADD4:
            OBJ = MovingObstacles(-100, 220, PLAYER)
            ENEMIES.add(OBJ)
            ALL_SPRITES.add(OBJ)
        if event.type == ADD5:
            OBJ = MovingObstacles(-100, 62, PLAYER)
            ENEMIES.add(OBJ)
            ALL_SPRITES.add(OBJ)

    # Image for background
    PIC1 = pygame.image.load('images/river_alt.jpg').convert()
    PIC1 = pygame.transform.scale(PIC1, (SCREEN_WIDTH, SCREEN_HEIGHT))
    RECT = (0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)
    SCREEN.blit(PIC1, RECT)
    # Image for paths in middle
    PIC1 = pygame.image.load('images/dirt.jpg').convert()
    PIC1 = pygame.transform.scale(PIC1, (SCREEN_WIDTH, 24))
    RECT = (0, SCREEN_HEIGHT - 24, SCREEN_WIDTH, 24)
    SCREEN.blit(PIC1, RECT)
    RECT = (0, SCREEN_HEIGHT - 164, SCREEN_WIDTH, 24)
    SCREEN.blit(PIC1, RECT)
    RECT = (0, SCREEN_HEIGHT - 324, SCREEN_WIDTH, 24)
    SCREEN.blit(PIC1, RECT)
    RECT = (0, SCREEN_HEIGHT - 484, SCREEN_WIDTH, 24)
    SCREEN.blit(PIC1, RECT)
    RECT = (0, 0, SCREEN_WIDTH, 24)
    SCREEN.blit(PIC1, RECT)
    RECT = (0, SCREEN_HEIGHT - 640, SCREEN_WIDTH, 24)
    SCREEN.blit(PIC1, RECT)

    # Display score, time, start and end.
    if PLAYER.player_id == 1:
        TXT_TO_SHOW = MYFONT.render(
            'Score: ' + str(PLAYER.score1), True, BLACK)
        SCREEN.blit(TXT_TO_SHOW, (10, 0))
        TXT_TO_SHOW = MYFONT.render('Start', True, BLACK)
        SCREEN.blit(TXT_TO_SHOW, (SCREEN_WIDTH/2 - 76, SCREEN_HEIGHT-28))
        TXT_TO_SHOW = MYFONT.render('End', True, BLACK)
        SCREEN.blit(TXT_TO_SHOW, (SCREEN_WIDTH/2 - 76, -1))
    else:
        TXT_TO_SHOW = MYFONT.render(
            'Score: ' + str(PLAYER.score2), True, BLACK)
        SCREEN.blit(TXT_TO_SHOW, (10, 0))
        TXT_TO_SHOW = MYFONT.render('End', True, BLACK)
        SCREEN.blit(TXT_TO_SHOW, (SCREEN_WIDTH/2 - 76, SCREEN_HEIGHT-28))
        TXT_TO_SHOW = MYFONT.render('Start', True, BLACK)
        SCREEN.blit(TXT_TO_SHOW, (SCREEN_WIDTH/2 - 76, -1))
    TXT_TO_SHOW = MYFONT.render('Time: ' + str(TIME//60), True, BLACK)
    SCREEN.blit(TXT_TO_SHOW, (SCREEN_WIDTH - 100, 0))

    TXT_TO_SHOW = MYFONT.render(
        'Player 1: ' + str(PLAYER.wins1), True, BLACK)
    SCREEN.blit(TXT_TO_SHOW, (10, SCREEN_HEIGHT - 27))
    TXT_TO_SHOW = MYFONT.render(
        'Player 2: ' + str(PLAYER.wins2), True, BLACK)
    SCREEN.blit(TXT_TO_SHOW, (SCREEN_WIDTH - 140, SCREEN_HEIGHT - 27))
    KEYS = pygame.key.get_pressed()

    # Check if PLAYER collides with any sprite in group ENEMIES,
    # and reset if collision occurs.
    if pygame.sprite.spritecollideany(PLAYER, ENEMIES):
        pygame.mixer.music.stop()
        DEATH_SOUND.play()
        if PLAYER.player_id == 2:
            reset_2()
        else:
            reset_1()
        pygame.mixer.music.play(-1)

    # Update PLAYER position based on Keys pressed
    STATE = PLAYER.update(KEYS, STATE)

    # Update position of ENEMIES
    for OBJ in ENEMIES:
        OBJ.update()

    # Blit all sprites to screen
    for sprites in ALL_SPRITES:
        SCREEN.blit(sprites.surf, sprites.rect)

    # Update score of the PLAYER
    update_score(PLAYER)

    pygame.display.flip()
