BLACK = (0, 0, 0)

WHITE = (255, 255, 255)

font = 'spacemono'

player_1_succ = 'Player 1 Wins'

player_2_succ = 'Player 2 Wins'

player_tie = 'Tie!!!'

next_turn = 'Player 2\'s turn'